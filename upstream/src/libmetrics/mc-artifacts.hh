/*
 *       File name:  libmetrics/mc-artifacts.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *
 * Initial version:  2012-10-21
 *
 *         Purpose:  artifacts, MC-based
 *
 *         License:  GPL
 */

#ifndef AGH_LIBMETRICS_MC_ARTIFACTS_H_
#define AGH_LIBMETRICS_MC_ARTIFACTS_H_

#include <vector>
#include <valarray>
#include "common/lang.hh"
#include "libsigproc/sigproc.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace metrics {
namespace mc {

struct SArtifactDetectionPPack {
        double  scope,
                upper_thr, lower_thr,
                f0, fc, bandwidth,
                mc_gain, iir_backpolate;
        double  E, dmin, dmax;
        size_t  sssu_hist_size,
                smooth_side;
        bool    estimate_E,
                use_range;

        bool
        operator==( const SArtifactDetectionPPack& rv) const
                {
                        using agh::dbl_cmp;
                        return  dbl_cmp(scope, rv.scope)                   == 0 &&
                                dbl_cmp(upper_thr, rv.upper_thr)           == 0 &&
                                dbl_cmp(lower_thr, rv.lower_thr)           == 0 &&
                                dbl_cmp(f0, rv.f0)                         == 0 &&
                                dbl_cmp(fc, rv.fc)                         == 0 &&
                                dbl_cmp(bandwidth, rv.bandwidth)           == 0 &&
                                dbl_cmp(mc_gain, rv.mc_gain)               == 0 &&
                                dbl_cmp(iir_backpolate, rv.iir_backpolate) == 0 &&
                                dbl_cmp(E, rv.E)                           == 0 &&
                                dbl_cmp(dmin, rv.dmin)                     == 0 &&
                                dbl_cmp(dmax, rv.dmax)                     == 0 &&
                                sssu_hist_size                             == rv.sssu_hist_size &&
                                smooth_side                                == rv.smooth_side &&
                                estimate_E                                 == rv.estimate_E &&
                                use_range                                  == rv.use_range;
                }
};

inline void
make_default_SArtifactDetectionPPack( SArtifactDetectionPPack& p)
{
        p.scope          =  4.;
        p.upper_thr      =  9.;
        p.lower_thr      = -9.;
        p.f0             =  1.;
        p.fc             =  1.8;
        p.bandwidth      =  1.5;
        p.mc_gain        = 10.;
        p.iir_backpolate =   .5;
        p.E              =  4.;
        p.dmin           = -10;
        p.dmax           =  20;
        p.sssu_hist_size = 100;
        p.smooth_side    =   0;
        p.estimate_E     =  true;
        p.use_range      = false;
}



template <typename T>
vector<size_t> // don't estimate, use pi*B*x^2 (E) as provided
detect_artifacts( const valarray<T>& signal, size_t sr,
                  const SArtifactDetectionPPack& P);


template <typename T>
double
estimate_E( const valarray<T>&,
            size_t bins,
            double dmin, double dmax);

template <typename T>
inline double
estimate_E( const valarray<T>& sssu_diff,
            size_t sssu_hist_size)
{
        return estimate_E( sssu_diff, sssu_hist_size,
                           sssu_diff.min(), sssu_diff.max());
}


#include "mc-artifacts.ii"


} // namespace mc
} // namespace metrics


#endif
