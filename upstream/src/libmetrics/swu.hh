/*
 *       File name:  libmetrics/swu.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *
 * Initial version:  2012-11-10
 *
 *         Purpose:  CBinnedSWU and related stuff
 *
 *         License:  GPL
 */

#ifndef AGH_LIBMETRICS_SWU_H_
#define AGH_LIBMETRICS_SWU_H_

#include <string>
#include <list>
#include <valarray>

#include "forward-decls.hh"
#include "page-metrics-base.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace metrics {
namespace swu {


struct SPPack
  : virtual public metrics::SPPack {

        double  min_upswing_duration;

        SPPack ()
                {
                        sane_defaults();
                }

        SPPack (const SPPack& rv)
              : metrics::SPPack      (rv),
                min_upswing_duration (rv.min_upswing_duration)
                {}

        SPPack (const metrics::SPPack& base,
                const double min_upswing_duration_)
              : metrics::SPPack      (base),
                min_upswing_duration (min_upswing_duration_)
                {}

        size_t
        compute_n_bins( size_t) const
                { return 1; }

        void
        check() const;

        void
        sane_defaults()
                {
                        metrics::SPPack::sane_defaults();
                        min_upswing_duration = 1.;
                }

        const char*
        metric_name() const
                { return metrics::name( TType::swu); }
};



class CProfile
  : virtual public SPPack,
    virtual public metrics::CProfile {

    public:
        CProfile (const sigfile::CTypedSource&, int sig_no,
                  const SPPack&);

        valarray<TFloat> course( double) const
                {
                        size_t  bin = 0; // (size_t)(binf - freq_from / Pp.freq_inc);
                        return metrics::CProfile::course(bin);
                }

        int go_compute();

        string fname_base() const;
        string mirror_fname() const;

        int export_tsv( const string&) const;
        int export_tsv( float, float,
                        const string&) const;

        // // to enable use as mapped type
        // CProfile (const CProfile& rv)
        //       : SPPack (rv),
        //         metrics::CProfile (rv)
        //         {}
};

} // namespace swu
} // namespace metrics


#endif
