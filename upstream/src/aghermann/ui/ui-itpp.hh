/*
 *       File name:  aghermann/ui/ui-itpp.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-21
 *
 *         Purpose:  simple, C-style UI supporting functions (those which need itpp)
 *
 *         License:  GPL
 */


#ifndef AGH_AGHERMANN_UI_UI_ITPP_H_
#define AGH_AGHERMANN_UI_UI_ITPP_H_

#include <itpp/base/mat.h>
#include "ui.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace agh {
namespace ui {


inline void
cairo_draw_signal( cairo_t *cr,
                   const itpp::Mat<double>& signal, int row,
                   ssize_t start, ssize_t end,
                   size_t width, double hdisp, double vdisp, float display_scale,
                   unsigned short decimate = 1,
                   TDrawSignalDirection direction = TDrawSignalDirection::forward,
                   TDrawSignalPathOption continue_path = TDrawSignalPathOption::yes)
{
        valarray<TFloat> tmp (end - start); // avoid copying other rows, cols
        for ( ssize_t c = 0; c < (end-start); ++c )
                if ( likely (start + c > 0 && start + c < (ssize_t)signal.size()) )
                        tmp[c] = signal(row, start + c);
        cairo_draw_signal( cr,
                           tmp, 0, end-start,
                           width, hdisp, vdisp, display_scale,
                           decimate,
                           direction,
                           continue_path);
}


}
} // namespace agh::ui

#endif
