/*
 *       File name:  aghermann/ui/sf/d/artifacts.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-01-24
 *
 *         Purpose:  scoring facility Artifacts dialog
 *
 *         License:  GPL
 */

#ifndef AGH_AGHERMANN_UI_SF_D_ARTIFACTS_H_
#define AGH_AGHERMANN_UI_SF_D_ARTIFACTS_H_

#include <list>

#include <gtk/gtk.h>

#include "aghermann/artifact-detection/3in1.hh"
#include "aghermann/ui/dirlevel-storable-adapter.hh"
#include "aghermann/ui/ui++.hh"
#include "aghermann/ui/sf/sf.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace agh {
namespace ui {

struct SScoringFacility::SArtifactsDialog
  : public SDirlevelStorableAdapter<agh::ad::CComprehensiveArtifactDetector> {

        DELETE_DEFAULT_METHODS (SArtifactsDialog);

        SArtifactsDialog (SScoringFacility&);
       ~SArtifactsDialog ();

        SScoringFacility::SChannel
                *using_channel;
        sigfile::SArtifacts
                artifacts_backup;
        bool    orig_signal_visible_backup;
        list<pair<SScoringFacility::SChannel*, bool>>
                channels_visible_backup;
        bool    suppress_preview_handler;

        SScoringFacility&
                _p;

        GtkBuilder
                *builder;
      // widgets
        GtkDialog
                *wSFAD;
        // A. Profiles
        GtkListStore
                *mSFADProfiles;
        GtkComboBox
                *eSFADProfileList;
        gulong  eSFADProfileList_changed_cb_handler_id;
        GtkButton
                *bSFADProfileSave,
                *bSFADProfileRevert,
                *bSFADProfileDiscard;
        GtkDialog
                *wSFADProfileSave;
        GtkToggleButton
                *eSFADProfileSaveOriginSubject,
                *eSFADProfileSaveOriginExperiment,
                *eSFADProfileSaveOriginUser;
        GtkButton
                *bSFADProfileSaveOK;
        GtkEntry
                *eSFADProfileSaveName;

        // B. Contents
        // 1. MC-based
        GtkExpander
                *cSFADMCBasedExpander;
        GtkLabel
                *lSFADMCBasedSummary;
        GtkTable
                *cSFADMCBased;
        GtkCheckButton
                *eSFADMCBasedConsider;
        GtkSpinButton
                *eSFADUpperThr,
                *eSFADLowerThr,
                *eSFADScope,
                *eSFADF0,
                *eSFADFc,
                *eSFADBandwidth,
                *eSFADMCGain,
                *eSFADBackpolate,
                *eSFADEValue,
                *eSFADHistRangeMin,
                *eSFADHistRangeMax,
                *eSFADHistBins,
                *eSFADSmoothSide;
        GtkCheckButton
                *eSFADEstimateE,
                *eSFADSingleChannelPreview;
        GtkRadioButton
                *eSFADUseThisRange,
                *eSFADUseComputedRange;
        GtkTable
                *cSFADWhenEstimateEOn,
                *cSFADWhenEstimateEOff;

        // 2. Flat regions
        GtkExpander
                *cSFADFlatExpander;
        GtkLabel
                *lSFADFlatSummary;
        GtkTable
                *cSFADFlat;
        GtkCheckButton
                *eSFADFlatConsider;
        GtkSpinButton
                *eSFADFlatMinRegionSize,
                *eSFADFlatPad;

        // 3. EMG perturbations
        GtkExpander
                *cSFADEMGExpander;
        GtkLabel
                *lSFADEMGSummary;
        GtkTable
                *cSFADEMG;
        GtkCheckButton
                *eSFADEMGConsider;
        GtkSpinButton
                *eSFADEMGMinSteadyToneFactor,
                *eSFADEMGMinSteadyToneRun;

        // C. Dialog buttons
        GtkLabel
                *lSFADInfo,
                *lSFADDirtyPercent;
        GtkToggleButton
                *bSFADPreview;
        GtkButton
                *bSFADApply,
                *bSFADDismiss;
};


}
} // namespace agh::ui


extern "C" {
gboolean wSFAD_delete_event_cb(GtkWidget*, GdkEvent*, gpointer);
void wSFAD_close_cb(GtkWidget*, gpointer);
void wSFAD_show_cb(GtkWidget*, gpointer);
void eSFADProfileList_changed_cb( GtkComboBox*, gpointer);
void bSFADProfileSave_clicked_cb( GtkButton*, gpointer);
void bSFADProfileRevert_clicked_cb( GtkButton*, gpointer);
void bSFADProfileDiscard_clicked_cb( GtkButton*, gpointer);

void cSFADMCBasedExpander_activate_cb( GtkExpander*, gpointer);
void cSFADFlatExpander_activate_cb( GtkExpander*, gpointer);
void cSFADEMGExpander_activate_cb( GtkExpander*, gpointer);
void eSFADMCBasedConsider_toggled_cb( GtkToggleButton*, gpointer);
void eSFADFlatConsider_toggled_cb( GtkToggleButton*, gpointer);
void eSFADEMGConsider_toggled_cb( GtkToggleButton*, gpointer);

void eSFAD_any_profile_value_changed_cb( GtkSpinButton*, gpointer);
void eSFAD_any_profile_origin_toggled_cb(GtkRadioButton*, gpointer);

void eSFADEstimateE_toggled_cb( GtkToggleButton*, gpointer);
void eSFADUseThisRange_toggled_cb( GtkToggleButton*, gpointer);
void bSFADPreview_toggled_cb( GtkToggleButton*, gpointer);
void bSFADApply_clicked_cb( GtkButton*, gpointer);
void bSFADDismiss_clicked_cb( GtkButton*, gpointer);
}

#endif
