/*
 *       File name:  aghermann/ui/sf/d/artifacts.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2012-10-05
 *
 *         Purpose:  scoring facility: artifact detection dialog
 *
 *         License:  GPL
 */

#include "aghermann/ui/mw/mw.hh"
#include "artifacts.hh"

using namespace std;
using namespace agh::ui;

SScoringFacility::SArtifactsDialog&
SScoringFacility::
artifacts_d()
{
        if ( not _artifacts_d )
                _artifacts_d = new SArtifactsDialog(*this);
        return *_artifacts_d;
}


SScoringFacility::SArtifactsDialog::
SArtifactsDialog (SScoringFacility& p_)
      : SDirlevelStorableAdapter<agh::ad::CComprehensiveArtifactDetector> (
              *p_._p.ED, agh::SExpDirLevelId {p_._p.ED->group_of(p_.csubject()), p_.csubject().id, p_.session()},
              mSFADProfiles, eSFADProfileList, eSFADProfileList_changed_cb_handler_id,
              bSFADProfileSave, bSFADProfileRevert, bSFADProfileDiscard,
              wSFADProfileSave, eSFADProfileSaveName,
              eSFADProfileSaveOriginSubject, eSFADProfileSaveOriginExperiment, eSFADProfileSaveOriginUser,
              bSFADProfileSaveOK),
        using_channel (nullptr),
        _p (p_)
{
      // 1. widgets
        builder = gtk_builder_new();
        if ( !gtk_builder_add_from_resource( builder, "/org/gtk/aghermann/sf-artifacts.glade", NULL) )
                throw runtime_error( "Failed to load SF::artifacts glade resource");
        gtk_builder_connect_signals( builder, NULL);

        AGH_GBGETOBJ (wSFAD);

        AGH_GBGETOBJ (eSFADProfileList);
        AGH_GBGETOBJ (bSFADProfileSave);
        AGH_GBGETOBJ (bSFADProfileRevert);
        AGH_GBGETOBJ (bSFADProfileDiscard);
        AGH_GBGETOBJ (wSFADProfileSave);
        AGH_GBGETOBJ (eSFADProfileSaveName);
        AGH_GBGETOBJ (eSFADProfileSaveOriginSubject);
        AGH_GBGETOBJ (eSFADProfileSaveOriginExperiment);
        AGH_GBGETOBJ (eSFADProfileSaveOriginUser);
        AGH_GBGETOBJ (bSFADProfileSaveOK);

        AGH_GBGETOBJ (cSFADMCBasedExpander);
        AGH_GBGETOBJ (lSFADMCBasedSummary);
        AGH_GBGETOBJ (eSFADMCBasedConsider);
        AGH_GBGETOBJ (cSFADMCBased);
        AGH_GBGETOBJ (eSFADScope);
        AGH_GBGETOBJ (eSFADUpperThr);
        AGH_GBGETOBJ (eSFADLowerThr);
        AGH_GBGETOBJ (eSFADF0);
        AGH_GBGETOBJ (eSFADFc);
        AGH_GBGETOBJ (eSFADBandwidth);
        AGH_GBGETOBJ (eSFADMCGain);
        AGH_GBGETOBJ (eSFADBackpolate);
        AGH_GBGETOBJ (eSFADEValue);
        AGH_GBGETOBJ (eSFADHistRangeMin);
        AGH_GBGETOBJ (eSFADHistRangeMax);
        AGH_GBGETOBJ (eSFADHistBins);
        AGH_GBGETOBJ (eSFADSmoothSide);
        AGH_GBGETOBJ (eSFADSingleChannelPreview);
        AGH_GBGETOBJ (eSFADEstimateE);
        AGH_GBGETOBJ (eSFADUseThisRange);
        AGH_GBGETOBJ (eSFADUseComputedRange);
        AGH_GBGETOBJ (cSFADWhenEstimateEOn);
        AGH_GBGETOBJ (cSFADWhenEstimateEOff);

        AGH_GBGETOBJ (cSFADFlatExpander);
        AGH_GBGETOBJ (lSFADFlatSummary);
        AGH_GBGETOBJ (eSFADFlatConsider);
        AGH_GBGETOBJ (cSFADFlat);
        AGH_GBGETOBJ (eSFADFlatMinRegionSize);
        AGH_GBGETOBJ (eSFADFlatPad);

        AGH_GBGETOBJ (cSFADEMGExpander);
        AGH_GBGETOBJ (lSFADEMGSummary);
        AGH_GBGETOBJ (eSFADEMGConsider);
        AGH_GBGETOBJ (cSFADEMG);
        AGH_GBGETOBJ (eSFADEMGMinSteadyToneFactor);
        AGH_GBGETOBJ (eSFADEMGMinSteadyToneRun);

        AGH_GBGETOBJ (lSFADInfo);
        AGH_GBGETOBJ (lSFADDirtyPercent);
        AGH_GBGETOBJ (bSFADPreview);
        AGH_GBGETOBJ (bSFADApply);
        AGH_GBGETOBJ (bSFADDismiss);

        mSFADProfiles = gtk_list_store_new( 1, G_TYPE_STRING);
        // this GtkListStore is populated from the same source, but something
        // haunting GTK+ forbids reuse of _p.mGlobalArtifactDetectionProfiles
        gtk_combo_box_set_model_properly( eSFADProfileList, mSFADProfiles);

        G_CONNECT_1 (wSFAD, show);
        G_CONNECT_1 (wSFAD, close);
        G_CONNECT_2 (wSFAD, delete, event);

        eSFADProfileList_changed_cb_handler_id =
                G_CONNECT_1 (eSFADProfileList, changed);

        G_CONNECT_1 (bSFADProfileSave, clicked);
        G_CONNECT_1 (bSFADProfileRevert, clicked);
        G_CONNECT_1 (bSFADProfileDiscard, clicked);

        G_CONNECT_1 (cSFADMCBasedExpander, activate);
        G_CONNECT_1 (cSFADFlatExpander, activate);
        G_CONNECT_1 (cSFADEMGExpander, activate);
        G_CONNECT_1 (eSFADMCBasedConsider, toggled);
        G_CONNECT_1 (eSFADFlatConsider, toggled);
        G_CONNECT_1 (eSFADEMGConsider, toggled);

        for ( auto& W : {eSFADUpperThr, eSFADLowerThr, eSFADScope,
                         eSFADF0, eSFADFc, eSFADBandwidth, eSFADMCGain, eSFADBackpolate, eSFADEValue,
                         eSFADHistRangeMin, eSFADHistRangeMax, eSFADHistBins, eSFADSmoothSide,
                         eSFADFlatMinRegionSize, eSFADFlatPad,
                         eSFADEMGMinSteadyToneFactor, eSFADEMGMinSteadyToneRun} )
                g_signal_connect(
                        W, "value-changed",
                        (GCallback)eSFAD_any_profile_value_changed_cb,
                        this);

        G_CONNECT_1 (eSFADEstimateE, toggled);
        G_CONNECT_1 (eSFADUseThisRange, toggled);

        G_CONNECT_1 (bSFADPreview, toggled);
        G_CONNECT_1 (bSFADApply, clicked);
        G_CONNECT_1 (bSFADDismiss, clicked);

        for ( auto& W : {eSFADProfileSaveOriginUser, eSFADProfileSaveOriginExperiment, eSFADProfileSaveOriginSubject} )
                g_signal_connect(
                        W, "toggled",
                        (GCallback)eSFAD_any_profile_origin_toggled_cb,
                        this);
      // 2. vars
        auto& P = Pp2.Pp;
        W_V.reg( eSFADMCBasedConsider,               &P.do_mc_based);
        W_V.reg( eSFADScope,                         &P.MC.scope);
        W_V.reg( eSFADUpperThr,                      &P.MC.upper_thr);
        W_V.reg( eSFADLowerThr,                      &P.MC.lower_thr);
        W_V.reg( eSFADF0,                            &P.MC.f0);
        W_V.reg( eSFADFc,                            &P.MC.fc);
        W_V.reg( eSFADBandwidth,                     &P.MC.bandwidth);
        W_V.reg( eSFADMCGain,                        &P.MC.mc_gain);
        W_V.reg( eSFADBackpolate,                    &P.MC.iir_backpolate);
        W_V.reg( eSFADEstimateE,                     &P.MC.estimate_E);
        W_V.reg( eSFADEValue,                        &P.MC.E);
        W_V.reg( eSFADHistRangeMin,                  &P.MC.dmin);
        W_V.reg( eSFADHistRangeMax,                  &P.MC.dmax);
        W_V.reg( eSFADHistBins,     (int*)           &P.MC.sssu_hist_size);
        W_V.reg( eSFADSmoothSide,   (int*)           &P.MC.smooth_side);
        W_V.reg( (GtkCheckButton*)eSFADUseThisRange, &P.MC.use_range);
        W_V.reg( eSFADFlatMinRegionSize,             &P.flat_min_size);
        W_V.reg( eSFADFlatPad,                       &P.flat_pad);

        W_V.reg( eSFADEMGConsider,                   &P.do_emg_perturbations);
        W_V.reg( eSFADEMGMinSteadyToneFactor,        &P.emg_min_steadytone_factor);
        W_V.reg( eSFADEMGMinSteadyToneRun,           &P.emg_min_steadytone_run);

        W_V.reg( eSFADFlatConsider,                  &P.do_flat_regions);
        W_V.reg( eSFADFlatMinRegionSize,             &P.flat_min_size);
        W_V.reg( eSFADFlatPad,                       &P.flat_pad);

        protected_up();
}


SScoringFacility::SArtifactsDialog::
~SArtifactsDialog ()
{
        gtk_widget_destroy( (GtkWidget*)wSFADProfileSave);
        gtk_widget_destroy( (GtkWidget*)wSFAD);
        g_object_unref( (GObject*)mSFADProfiles);
        g_object_unref( (GObject*)builder);
}
