/*
 *       File name:  aghermann/main.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2010-05-03
 *
 *         Purpose:  Function main
 *
 *         License:  GPL
 */


#include <cstdio>

#ifdef _OPENMP
# include <omp.h>
#endif

#include <gtk/gtk.h>

#include "project_strings.h"
#include "globals.hh"
#include "expdesign/expdesign.hh"
#include "ui/globals.hh"
#include "ui/ui.hh"
#include "ui/sm/sm.hh"

using namespace std;
using namespace agh;

void print_version();

namespace {
void print_usage( const char*);
}


int
main( int argc, char **argv)
{
        puts( PACKAGE_STRING "\n");

        bool headless = false;
        string lf_fname = "";
        int c;
        while ( (c = getopt( argc, argv, "hnl:")) != -1 )
                switch ( c ) {
                case 'n': // headless
                        headless = true;
                        break;
                case 'l':
                        lf_fname.assign( optarg);
                        break;
                case 'h':
                        print_usage( argv[0]);
                        return 0;
                }

        if ( headless ) {
                char*& explicit_session = argv[optind];
                if ( (!explicit_session || strlen(explicit_session) == 0) ) {
                        fprintf( stderr, "Headless mode requires explicit session dir\n");
                        print_usage( argv[0]);
                        return -1;
                }

                global::init( lf_fname);
                CExpDesign ED (explicit_session); // essentially a very thoughtful no-op
                global::fini();

        } else {
                gtk_init( &argc, &argv);

                agh::global::init( lf_fname);

                if ( agh::ui::global::prepare_for_expdesign() ) {
                        ui::pop_ok_message(
                                NULL,
                                "UI failed to initialize",
                                "Your install is broken.");
                        return 2;
                }

                ui::SSessionChooser chooser (argv[optind]);
                // implicit read sessionrc, run

                gtk_main();

                agh::global::fini();
                // g_object_unref (app); // abandon ship anyway
        }

        return 0;
}

namespace {
void
print_usage( const char* argv0)
{
        printf( "Usage: %s [-n] [-l log_file] [exp_root_dir]\n", argv0);
}
} // namespace
