/*
 *       File name:  aghermann/expdesign/loadsave.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2008-04-28
 *
 *         Purpose:  CExpDesign::{load,save}.
 *
 *         License:  GPL
 */

#include "project_strings.h"
#include "aghermann/globals.hh"
#include "aghermann/model/achermann.hh"
#include "expdesign.hh"


using namespace std;
using namespace agh;



int
CExpDesign::
load_settings()
{
        libconfig::Config conf;

        try {
                conf.readFile( EXPD_FILE);

                config.get( conf, agh::global::default_log_facility, agh::TThrowOption::do_throw);

                for ( size_t t = 0; t < ach::TTunable::_basic_tunables; ++t ) {
                        auto& A = conf.lookup(string("tunable.") + ach::tunable_name(t));
                        tunables0[t] = A[0];
                        tlo      [t] = A[1];
                        thi      [t] = A[2];
                        tstep    [t] = A[3];
                }
        } catch (...) {
                APPLOG_INFO ("load_settings(\"%s\"): File not readable or has invalid contents", EXPD_FILE);

                _status = _status | load_fail;

                ctl_params0.reset();
                tunables0.set_defaults();
                psd_params.sane_defaults();
                swu_params.sane_defaults();
                mc_params.sane_defaults();

                return -1;
        }

        try {
                for ( size_t i = metrics::TBand::delta; i < metrics::TBand::TBand_total; ++i ) {
                        auto& A = conf.lookup(string("Band.")+FreqBandNames[i]);
                        float   f0 = A[0],
                                f1 = A[1];
                        if ( f0 < f1 ) {
                                freq_bands[i][0] = f0;
                                freq_bands[i][1] = f1;
                        } else
                                APPLOG_WARN ("load_settings(\"%s\"): Invalid Band range", EXPD_FILE);
                }
        } catch (...) {
                APPLOG_WARN ("load_settings(\"%s\"): Something is wrong with Band section", EXPD_FILE);
        }

        try { ctl_params0.check(); }
        catch (...) {
                ctl_params0.reset();
                APPLOG_WARN ("load_settings(\"%s\"): Invalid ctl parameters", EXPD_FILE);
        }

        if ( tunables0.check() ) {
                tunables0.set_defaults();
                APPLOG_WARN ("load_settings(\"%s\"): Invalid tunables", EXPD_FILE);
        }

        try { psd_params.check(); }
        catch (invalid_argument ex) {
                psd_params.sane_defaults();
                APPLOG_WARN ("load_settings(\"%s\"): Invalid PSD params (%s)", EXPD_FILE, ex.what());
        }

        try { swu_params.check(); }
        catch (invalid_argument ex) {
                swu_params.sane_defaults();
                APPLOG_WARN ("load_settings(\"%s\"): Invalid SWU params (%s)", EXPD_FILE, ex.what());
        }

        try { mc_params.check(); }
        catch (invalid_argument ex) {
                mc_params.sane_defaults();
                APPLOG_WARN ("load_settings(\"%s\"): Invalid MC params (%s)", EXPD_FILE, ex.what());
        }

        return 0;
}





int
CExpDesign::
save_settings()
{
        libconfig::Config C;
        config.put( C, agh::global::default_log_facility);

        // only save _agh_basic_tunables_
        for ( size_t t = 0; t < ach::TTunable::_basic_tunables; ++t )
                confval::put( C, string("tunable.") + ach::tunable_name(t),
                              list<double> {tunables0[t], tlo[t], thi[t], tstep[t]});

        for ( unsigned i = metrics::TBand::delta; i < metrics::TBand::TBand_total; ++i )
                confval::put( C, string("Band.") + FreqBandNames[i],
                              list<double> {freq_bands[i][0], freq_bands[i][1]});

        C.writeFile( EXPD_FILE);

        return 0;
}
