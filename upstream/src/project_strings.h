/*
 *       File name:  project_strings.h
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-21
 *
 *         Purpose:  project-wide #define'd things
 *
 *         License:  GPL
 */


#ifndef AGH_PROJECT_STRINGS_H_
#define AGH_PROJECT_STRINGS_H_

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

#define AGH_UI_GRESOURCE_FILE "aghermann.gresource"
#define EXPD_FILE ".expdesign.conf"
#define CONF_FILE ".aghermann.conf"

#endif
