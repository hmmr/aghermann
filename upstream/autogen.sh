#!/bin/sh

test -d /usr/share/autoconf-archive && AUTOCONF_ARCHIVE="-I /usr/share/autoconf-archive"
test -d /usr/share/aclocal/ac-archive && AUTOCONF_ARCHIVE=$AUTOCONF_ARCHIVE" -I /usr/share/aclocal/ac-archive"
libtoolize --force --copy
aclocal $AUTOCONF_ARCHIVE
autoheader
automake --gnu --add-missing --copy
autoconf
